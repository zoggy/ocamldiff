# OCamldiff -- Diff parser and printer

[🌐 OCamldiff homepage](https://zoggy.frama.io/ocamldiff/)

OCamldiff is an OCaml library to parse and display diff results.

## Documentation

Documentation is available [here](https://zoggy.frama.io/ocamldiff/refdoc/ocamldiff/Odiff/index.html).

## Development

Development is hosted on [Framagit](https://framagit.org/zoggy/ocamldiff).

OCamldiff is released under LGPL3 license.

## Installation

The `ocamldiff` package is installable with opam:

```sh
$ opam install ocamldiff
```

Current state of OCamldiff can be installed with:

```sh
$ opam pin add ocamldiff git@framagit.org:zoggy/ocamldiff.git
```
